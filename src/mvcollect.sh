#!/usr/bin/env bbrun

### mvcollect Usage:help
#
# mvcollect -a @NAME DIRPATH
# mvcollect -l
# mvcollect -d @NAME
#
# mvc -u COMMAND ...
#
# mvcollect @NAME
# mvcollect [@NAME] PATHS ...
#
#
#
# Register a short name to associate to a destination directory path DIRPATH using '-a'.
#
# List registered short names and paths with -l
#
# Delete a registered short name using '-d'.
#
# Using '-u' : attempt to undo a 'mv -i ...' command as printed by mvcollect.
#
#
#
# Change the default path to use by calling 'mvcollect' and a name, without specifying paths.
#
# Move items on PATHS into the DIRPATH specified by NAME.
# Temporarily use a different NAME, when specified with paths.
#
###/doc

#%include std/safe.sh
#%include std/out.sh
#%include std/autohelp.sh

#%include collector.sh
#%include movers.sh

MVC_configd="$HOME/.local/config/mvcollect"
MVC_configf="$MVC_configd/stores"
MVC_mvregister="$MVC_configd/last_move"

$%function mvc:main() {
    mvc:ensure_config

    mvc:check_mode "$1"

    if [[ "$MVC_mode" = a ]]; then
        shift
        mvc:set_collector "${1:1}" "$2"
    elif [[ "$MVC_mode" = d ]]; then
        shift
        mvc:del_collector "$1"
    elif [[ "$MVC_mode" = u ]]; then
        shift
        mvc:undo_collect "$@"
    elif [[ "$MVC_mode" = l ]]; then
        sed -r 's|^/\s+|(default)\t|' "$MVC_configf" | sort
    else
        if [[ "${1:0:1}" = "@" ]]; then
            mvc:load_collector "${1:1}"
            shift
        else
            mvc:load_collector
        fi

        if [[ -z "$*" ]]; then
            mvc:set_collector '/' "$MVC_destination"
        else
            mvc:collect_items "$@"
        fi
    fi
}

$%function mvc:check_mode(modename) {
    MVC_mode=""
    if [[ "${modename:0:1}" = "-" ]]; then
        MVC_mode="${modename:1}"
    fi
}

$%function mvc:ensure_config() {
    if [[ ! -d "$MVC_configd" ]]; then
        mkdir -p "$MVC_configd"
    fi

    if [[ ! -f "$MVC_configf" ]]; then
        touch "$MVC_configf"
    fi
}

autohelp:check-or-null "$@"
mvc:main "$@"
