$%function mvc:collect_items() {
    echo "$MVC_destination" > "$MVC_mvregister"
    /usr/bin/env ls -d "$@" >> "$MVC_mvregister" # Use 'ls' to ensure each path is on one line
    ( set -x
    mv -i "$@" "$MVC_destination/"
    )
}

$%function mvc:undo_collect() {
    local retrieve_from
    retrieve_from="$(head -n 1 "$MVC_mvregister")"

    local originfile
    while read originfile ; do
        local filename="$(basename "$originfile")"
        (set -x
        mv -i "$retrieve_from/$filename" "$originfile"
        )
    done < <(tail -n +2 "$MVC_mvregister")
}
