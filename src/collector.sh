$%function mvc:load_collector(?cname) {
    if [[ -n "$cname" ]]; then
        mvc:_load_collector_named "$cname"
    else
        mvc:_load_collector_named "/"
    fi
}

$%function mvc:_load_collector_named(cname) {
    local collector="$(grep -E "^$cname" "$MVC_configf" | sed -r 's|^\S+\s+||')"

    if [[ -n "$collector" ]] && [[ -d "$collector" ]]; then
        MVC_destination="$collector"
    else
        out:fail "Not a directory '$cname' : '$collector'"
    fi
}

$%function mvc:set_collector(cname path) {
    ([[ "$cname" =~ ^[a-zA-Z0-9_-]+$ ]] || [[ "$cname" = / ]]) ||
        out:fail "Collector shortnames can only contain ASCII letters and numbers."

    [[ -d "$path" ]] || out:fail "'$path' is not a directory."

    mvc:_set_collector_named "$cname" "$path"
    if ! grep -qE "^/\s+" "$MVC_configf" ; then
        mvc:_set_collector_named "/" "$path"
    fi
}

$%function mvc:_set_collector_named(cname path) {
    if grep -qE "^$cname\s+" "$MVC_configf" ; then
        sed -r "s|^$cname\s+.+|$cname\t$path|" -i "$MVC_configf"
    else
        echo -e "$cname\t$path" >> "$MVC_configf"
    fi
}

$%function mvc:del_collector(cname) {
    local tfile="$(mktemp)"
    grep -vE "^cname\s+" "$MVC_configf" > "$tfile" || :
    mv "$tfile" "$MVC_configf"
}
