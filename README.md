# `mvcollect` - move files to collection directories

Quick script to set a default destination to move files to.

Useful when doing cleaning up files... Example:

```sh
# Register some short names for long paths

mvcollect -a @sc "$HOME/Pictures/Screenshots"
mvcollect -a @mus "$HOME/Music/found_music"
mvcollect -a @ins "$HOME/Downloads/installer_files"

# Set the default short name to use
mvcollect @mus

cd SOME_PATH

# By default goes to the short named '@mus' which is the found_music folder
mvcollect *.mp3 *.ogg

cd SOME_OTHER_PATH

# Move screenshots into the screenshots folder for this one call
mvcollect @sc Screenshot*

# The default is still the '@mus' folder
mvcollect *.ogg *.mp3

```
